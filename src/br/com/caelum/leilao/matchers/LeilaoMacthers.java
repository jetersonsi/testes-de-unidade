package br.com.caelum.leilao.matchers;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

import br.com.caelum.leilao.dominio.Lance;
import br.com.caelum.leilao.dominio.Leilao;

public class LeilaoMacthers extends TypeSafeMatcher<Leilao> {

	private Lance lance;

	public LeilaoMacthers(Lance lance) {
		this.lance = lance;
	}
	
	@Override
	public void describeTo(Description arg0) {
		arg0.appendText("leilao com lance " + lance.getValor());
		
	}

	@Override
	protected boolean matchesSafely(Leilao item) {		
		return item.getLances().contains(lance);
	}

}
