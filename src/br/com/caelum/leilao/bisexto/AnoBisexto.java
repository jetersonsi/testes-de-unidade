package br.com.caelum.leilao.bisexto;

public class AnoBisexto {

	public boolean ehBisexto(int ano) {
		boolean bisexto;
		
		if(ano % 400 == 0) {
			bisexto = true;
		}else {
			if(ano % 4 == 0 && ano % 100 != 0) {
				bisexto = true;
			}else {
				bisexto = false;
			}
		}
		
		return bisexto;
	}
}
