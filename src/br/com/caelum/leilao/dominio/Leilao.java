package br.com.caelum.leilao.dominio;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Leilao {

	private String descricao;
	private List<Lance> lances;

	public Leilao(String descricao) {
		this.descricao = descricao;
		this.lances = new ArrayList<Lance>();
	}

	public void propoe(Lance lance) {

		
		if(lances.isEmpty() || podeDarLance(lance.getUsuario()))
			lances.add(lance);
	}

	private boolean podeDarLance(Usuario usuario) {
		return !ultimoLanceDado().getUsuario().equals(usuario) && quantidadeLancesDo(usuario) < 5;
	}
	
	public void dobrarLance(Usuario usuario) {
		if(ultimoLanceDo(usuario) != null) {
			Lance ultimo = ultimoLanceDo(usuario);
			Lance l = new Lance(ultimo.getUsuario(), ultimo.getValor() * 2);
			propoe(l);
		}
	}

	private int quantidadeLancesDo(Usuario usuario) {
		int total = 0;
		for(Lance l: lances) {
			if(l.getUsuario().equals(usuario)) total++;
		}
		return total;
	}
	
	private Lance ultimoLanceDo(Usuario usuario) {
		List<Lance> lancesDo = lances.stream().filter(l -> l.getUsuario().equals(usuario)).collect(Collectors.toList());
		if(lancesDo.size() > 0)
			return lancesDo.get(lancesDo.size()-1);
		return null;
	}

	private Lance ultimoLanceDado() {
		return lances.get(lances.size()-1);
	}



	public String getDescricao() {
		return descricao;
	}

	public List<Lance> getLances() {
		return Collections.unmodifiableList(lances);
	}



}
