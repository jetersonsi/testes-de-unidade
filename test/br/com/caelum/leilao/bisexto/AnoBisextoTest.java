package br.com.caelum.leilao.bisexto;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class AnoBisextoTest {

	@Test
	public void anoBisexto() {
		
		AnoBisexto b = new AnoBisexto();		
		assertEquals(true, b.ehBisexto(2016));
	}
	
	@Test
	public void anoNaoBisexto() {
		
		AnoBisexto b = new AnoBisexto();		
		assertEquals(false, b.ehBisexto(2011));
	}
}
