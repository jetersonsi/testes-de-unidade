package br.com.caelum.leilao.servico;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;

import org.hamcrest.Matcher;
import org.junit.Test;

import br.com.caelum.leilao.builder.CriadorDeLeilao;
import br.com.caelum.leilao.dominio.Lance;
import br.com.caelum.leilao.dominio.Leilao;
import br.com.caelum.leilao.dominio.Usuario;
import br.com.caelum.leilao.matchers.LeilaoMacthers;

public class LeilaoTest {

	@Test
	public void deveReceberUmLance() {
		Leilao leilao = new CriadorDeLeilao().para("Macbook Pro 15").constroi();
        assertEquals(0, leilao.getLances().size());

        Lance lance = new Lance(new Usuario("Steve Jobs"), 2000);
        leilao.propoe(lance);

        assertThat(leilao.getLances().size(), equalTo(1));
        assertThat(leilao, temUmLance(lance));
		
	}
	
	@Test
	public void deveReceverVariosLanecs() {
		Leilao l = new Leilao("Mcbook pro");				
		l.propoe(new Lance(new Usuario("Jobs"), 2000));
		l.propoe(new Lance(new Usuario("Wosniak"), 3000));
		
		assertEquals(2, l.getLances().size());
		assertEquals(2000, l.getLances().get(0).getValor(), 0.00001);
		assertEquals(3000, l.getLances().get(1).getValor(), 0.00001);
		
	}
	
	@Test
	public void naoDeveAceitarDoisLancesSeguidosMesmoUasuario(){
		Leilao l = new Leilao("Mcbook pro");	
		Usuario jobs = new Usuario("Jobs");
		
		l.propoe(new Lance(jobs, 2000));
		l.propoe(new Lance(jobs, 3000));
		
		assertEquals(1, l.getLances().size());
		assertEquals(2000, l.getLances().get(0).getValor(), 0.00001);
	}
	
	
	
	public static Matcher<Leilao> temUmLance(Lance lance) {
        return new LeilaoMacthers(lance);
    }
	
	@Test
	public void naoDeveAceitarMaisQueCincoLancesDoMesmoUsuario() {
		Leilao l = new Leilao("Mcbook pro");	
		Usuario jobs = new Usuario("Jobs");
		Usuario gates = new Usuario("Gates");
		
		l.propoe(new Lance(jobs, 2000));
		l.propoe(new Lance(gates, 3000));
		
		l.propoe(new Lance(jobs, 4000));
		l.propoe(new Lance(gates, 5000));
		
		l.propoe(new Lance(jobs, 6000));
		l.propoe(new Lance(gates, 7000));
		
		l.propoe(new Lance(jobs, 8000));
		l.propoe(new Lance(gates, 9000));
		
		l.propoe(new Lance(jobs, 10000));
		l.propoe(new Lance(gates, 11000));
		
		l.propoe(new Lance(jobs, 12000));
		
		
		assertEquals(10, l.getLances().size());
		assertEquals(11000, l.getLances().get(l.getLances().size()-1).getValor(), 0.00001);
	}
	
	@Test
	public void testeDobrarLance() {
		Leilao l = new Leilao("Mcbook pro");	
		Usuario jobs = new Usuario("Jobs");
		Usuario gates = new Usuario("Gates");
		
		l.propoe(new Lance(jobs, 2000));
		l.propoe(new Lance(gates, 3000));
		
		l.propoe(new Lance(jobs, 4000));
		l.propoe(new Lance(gates, 5000));
		
		l.propoe(new Lance(jobs, 6000));
		l.propoe(new Lance(gates, 7000));
		
		l.propoe(new Lance(jobs, 8000));
		l.propoe(new Lance(gates, 9000));
		
		l.propoe(new Lance(jobs, 10000));
		l.propoe(new Lance(gates, 11000));
				
		
		l.dobrarLance(jobs);
		
		
		assertEquals(10, l.getLances().size());
		assertEquals(11000, l.getLances().get(l.getLances().size()-1).getValor(), 0.00001);
		
		
	}
}
