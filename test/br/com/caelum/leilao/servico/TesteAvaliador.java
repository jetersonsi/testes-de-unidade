package br.com.caelum.leilao.servico;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import br.com.caelum.leilao.builder.CriadorDeLeilao;
import br.com.caelum.leilao.dominio.Lance;
import br.com.caelum.leilao.dominio.Leilao;
import br.com.caelum.leilao.dominio.Usuario;

public class TesteAvaliador {

	private Avaliador leiloeiro;
	private Usuario joao;
	private Usuario maria;
	
	

	
	


	@Before
	public void criaAvaliador() {
		leiloeiro = new Avaliador();

		joao = new Usuario("Jo�o");
		maria = new Usuario("Maria");
	}
	
	@Test(expected=RuntimeException.class)
	public void naoDeveAvalidarLeiloesSemLancesDados() {
		Leilao leilao = new CriadorDeLeilao().para("PreiStatchu 3").constroi();
		leiloeiro.avalia(leilao);				
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void naoDeveAceitarLanceMenorOuIgualAZero() {
		new CriadorDeLeilao().para("PreiStatchu 3").lance(maria, 0).constroi();
		new CriadorDeLeilao().para("PreiStatchu 3").lance(joao, -1).constroi();
	}
	
	

	@Test
	public void deveEncontrarOsTresMaioresLances() {

		Leilao leilao = new CriadorDeLeilao().para("Playstation 3 Novo")
				.lance(joao, 100)
				.lance(maria, 200)
				.lance(joao, 300)
				.lance(maria, 400)
				.constroi();

		

		leiloeiro.avalia(leilao);

		List<Lance> maiores = leiloeiro.getTresMaiores();

		assertThat(maiores.size(), equalTo(3));
		assertThat(maiores.get(0).getValor(), equalTo(400.0));
		assertThat(maiores.get(1).getValor(), equalTo(300.0));
		assertThat(maiores.get(2).getValor(), equalTo(200.0));
		
		
	}
	
	

	@Test
	public void deveDevolverTodosLancesCasoNaoHajaNoMinimo3() {

		Leilao leilao = new CriadorDeLeilao()
				.para("PreiStachu")
				.lance(joao, 100)
				.lance(maria, 200)
				.constroi();
			

		leiloeiro.avalia(leilao);

		List<Lance> maiores = leiloeiro.getTresMaiores();

		assertThat(maiores.size(), equalTo(2));
		assertThat(maiores.get(0).getValor(), equalTo(200.0));
		assertThat(maiores.get(1).getValor(), equalTo(100.0));
		
	}

	
}
